require 'net/http'
require 'json'
require 'pry'

class GeocodingService
  SERVICE_URL = 'https://maps.googleapis.com/maps/api/geocode/json?address='

  def initialize(query)
    @uri = URI("#{SERVICE_URL}#{query}&key=#{ENV['API_KEY']}")
  end

  def call
    JSON.parse(Net::HTTP.get(@uri))
  end
end
