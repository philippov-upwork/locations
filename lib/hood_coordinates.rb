require 'json'
require 'pry'
require 'nokogiri'
require_relative 'geocoding_service.rb'

class HoodCoordinates
  def initialize(list_file)
    @locations_json = JSON.parse(File.read(File.expand_path(list_file)))
  end

  def save_to_files
    csv = build_csv(locations_with_coordinates)
    plist = build_plist(locations_with_coordinates)

    write_file('data/results/location_data.csv', csv.join("\n"))
    write_file('data/results/location_data.plist', plist)

    write_alphabetical_plist
  end

  private

  def get_coordinates(query)
    puts "Calling GeocodingService with the query: #{query}"
    result = GeocodingService.new(query).call
    coordinates = result.dig('results', 0, 'geometry', 'location')

    puts "[Error] #{result}" unless coordinates

    coordinates
  end

  def write_alphabetical_plist
    locations_alphabetically(locations_with_coordinates).each do |letter, locations|
      plist = build_plist(locations)

      write_file("data/results/location_data_#{letter}.plist", plist)
    end
  end

  def locations_alphabetically(locations)
    locations.each_with_object({}) do |(hood, coordinates), memo|
      memo[hood[0].downcase] ||= {}
      memo[hood[0].downcase]["#{hood}"] = coordinates
    end
  end

  def locations_with_coordinates
    @locations_with_coordinates ||=
      build_location_queries.each_with_object({}) do |query, locations|
        coordinates = get_coordinates(query)
        hood = query.split(',')[0]

        locations[hood] = "#{coordinates['lat']},#{coordinates['lng']}" if coordinates
      end
  end

  def write_file(file, data)
    File.open(file, 'w') { |f| f.write(data) }
  end

  def build_csv(locations)
    locations.each_with_object([]) do |(hood, coordinates), memo|
      memo << "#{hood},#{coordinates}"
    end
  end

  def build_plist(locations)
    builder = Nokogiri::XML::Builder.new(encoding: 'UTF-8') do |xml|
      xml.doc.create_internal_subset(
        'plist',
        '-//Apple//DTD PLIST 1.0//EN',
        'http://www.apple.com/DTDs/PropertyList-1.0.dtd'
      )
      xml.plist(version: '1.0') do
        xml.dict do
          xml.key('US neighboorhood coordinates')
          xml.array do
            xml.dict do
              locations.each do |hood, coordinates|
                xml.key(hood)
                xml.string(coordinates)
              end
            end
          end
        end
      end
    end

    builder.to_xml
  end

  def build_location_queries
    locations = []

    @locations_json.each do |city, hoods|
      locations << build_location(city, '')

      hoods.each do |hood|
        locations << build_location(city, hood)
      end
    end

    locations
  end

  def build_location(city, hood)
    "#{hood} #{city}, United States".strip
  end
end

HoodCoordinates.new('data/locations.json').save_to_files
