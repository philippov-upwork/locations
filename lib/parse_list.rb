require 'nokogiri'
require 'json'

list = Nokogiri::HTML(open('../data/locations.html'))
locs = {}
last_city = nil

list.css('li ul > li').each do |el|
  if el.text.include?(',')
    last_city = el.text.split("\n")[0]
    locs[last_city] = []
    next
  end

  locs[last_city].push(el.text)
end

File.open('locations.json', 'w') { |file| file.write(locs.to_json) }
